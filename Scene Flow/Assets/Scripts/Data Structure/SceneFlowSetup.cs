﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;

public class SceneFlowSetup : ScriptableObject
{    
    [SerializeField, HideInInspector]
    private SceneAsset[] m_allScenes;
    
    [SerializeField, HideInInspector]
    private SceneFlowNode[] m_sceneNodes;

    [SerializeField, HideInInspector]
    private SceneFlowNode m_startNode;

    public void Awake()
    {
        if(m_allScenes == null)
        {
            m_allScenes = new SceneAsset[0];
        }

        if(m_sceneNodes == null)
        {
            m_sceneNodes = new SceneFlowNode[0];
        }
        
        if(m_startNode == null)
        {
            if (m_sceneNodes.Length > 0)
            {
                m_startNode = m_sceneNodes[0];
            }
            else
            {
                AssetDatabase.Refresh();

                if (AssetDatabase.Contains(this))
                {
                    m_startNode = AddSceneNode();
                }
                else
                {
                    Debug.Log("Not in assets");
                }
            }
        }           

        UpdateSceneAssets();                      
    }

    private void UpdateSceneAssets()
    {
        List<SceneAsset> assets = new List<SceneAsset>();

        if(m_startNode != null)
        {
            assets.Add(m_startNode.Scene);
        }

        for (int i = 0; i < m_sceneNodes.Length; i++)
        {
            SceneAsset scene = m_sceneNodes[i].Scene;
            
            if (scene != null && !assets.Contains(scene))
            {
                assets.Add(m_sceneNodes[i].Scene);
            }
        }

        m_allScenes = assets.ToArray();
    }

    public SceneFlowNode AddSceneNode()
    {
        SceneFlowNode node = CreateInstance<SceneFlowNode>();        
        node.hideFlags = HideFlags.HideInHierarchy;
        
        Undo.RegisterCreatedObjectUndo(node, "Add scene node");

        Undo.RegisterCompleteObjectUndo(this, "Add scene node");

        ArrayUtility.Add(ref m_sceneNodes, node);

        AssetDatabase.AddObjectToAsset(node, this);

        EditorUtility.SetDirty(this);

        UpdateSceneAssets();

        //Debug.Log("Added scene");

        return node;
    }

    public void RemoveSceneNode(SceneFlowNode node)
    {
        if(ArrayUtility.Contains(m_sceneNodes, node))
        {
            Undo.RegisterCompleteObjectUndo(this, "Delete Scene node");

            if (node == m_startNode)
            {
                if (m_sceneNodes.Length > 0)
                {
                    m_startNode = m_sceneNodes[0];
                }
                else
                {
                    return;
                }
            }

            SceneFlowNode temp = node;
                        
            ArrayUtility.Remove(ref m_sceneNodes, node);

            EditorUtility.SetDirty(this);
            
            UpdateSceneAssets();

            Undo.DestroyObjectImmediate(temp);
            
            //DestroyImmediate(temp, true);
        }
    }

    public string[] GetScenesLinkedToCurrent(string currentName)
    {
        List<string> sceneNames = new List<string>();

        SceneFlowNode node = null;

        for(int i = 0; i < m_sceneNodes.Length; i++)
        {
            if(m_sceneNodes[i].Scene.name == currentName)
            {
                node = m_sceneNodes[i];

                SceneFlowNode[] linked = node.GetLinkedScenes();

                //Debug.Log(linked.Length);

                for (int j = 0; j < linked.Length; j++)
                {
                    if (linked[j].Scene != null)
                    {                       
                        sceneNames.Add(linked[j].Scene.name);
                    }
                }

                break;
            }
        }        

        return sceneNames.ToArray();
    }

    public bool HasScene(string name)
    {        
        for(int i = 0; i < m_sceneNodes.Length; i++)
        {
            if(m_sceneNodes[i].Scene != null && m_sceneNodes[i].Scene.name == name)
            {
                return true;
            }
        }

        return false;            
    }

    public bool HasScene(SceneAsset scene)
    {
        for(int i = 0; i < m_allScenes.Length; i++)
        {
            if(scene == m_allScenes[i])
            {
                return true;
            }
        }

        return false;
    }

    public SceneFlowNode RegisterScene(SceneAsset scene)
    {
        if (!HasScene(scene))
        {
            ArrayUtility.Add(ref m_allScenes, scene);
        }

        SceneFlowNode node = AddSceneNode();

        return node;
    }

    public void RemoveScene(SceneAsset asset)
    {
        if(HasScene(asset))
        {
            for(int i = 0; i < m_sceneNodes.Length; i++)
            {
                if(m_sceneNodes[i].Scene == asset)
                {
                    m_sceneNodes[i].Scene = null;
                }                
            }
        }
    }

    [MenuItem("Assets/Create/New Scene Flow", priority = 1)]
    public static void CreateNewSetup()
    {
        string path = "Assets/";

        if(ProjectWindowUtil.IsFolder(Selection.activeObject.GetInstanceID()))
        {
            path = AssetDatabase.GetAssetPath(Selection.activeObject);
        }
        else if(AssetDatabase.Contains(Selection.activeObject))
        {
            path = System.IO.Path.GetDirectoryName(AssetDatabase.GetAssetPath(Selection.activeObject));            
        }

        path += "/NewSceneFlow.asset";

        SceneFlowSetup setup = CreateInstance<SceneFlowSetup>();        

        //ProjectWindowUtil.ShowCreatedAsset(setup);
        ProjectWindowUtil.StartNameEditingIfProjectWindowExists(setup.GetInstanceID(), CreateInstance<SetupFile>(), path, AssetPreview.GetMiniThumbnail(setup), null);
    }   

    private void CreateSetup()
    {
                
    }

    public SceneFlowNode StartNode
    {
        set
        {                     
            m_startNode = value;            
        }
        get
        {
            return m_startNode;
        }
    }

    public SceneFlowNode[] Nodes
    {
        get
        {
            return m_sceneNodes;
        }
    }

    public SceneAsset[] RegisteredScenes
    {
        get
        {
            UpdateSceneAssets();
            return m_allScenes;
        }
    }   
    
    private class SetupFile : UnityEditor.ProjectWindowCallback.EndNameEditAction
    {
        public override void Action(int instanceId, string pathName, string resourceFile)
        {
            pathName = AssetDatabase.GenerateUniqueAssetPath(pathName);

            AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(instanceId), pathName);

            AssetDatabase.Refresh();

            SceneFlowSetup setup = (SceneFlowSetup)EditorUtility.InstanceIDToObject(instanceId);

            setup.Awake();
        }
    }  
}