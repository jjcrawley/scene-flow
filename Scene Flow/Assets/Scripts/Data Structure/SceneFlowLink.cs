﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SceneFlowLink : ScriptableObject
{
    [SerializeField, HideInInspector]
    private SceneFlowNode m_destination;
    
    public SceneFlowNode Destination
    {
        set
        {
            Undo.RegisterCompleteObjectUndo(this, "Change link destination");
            m_destination = value;
            EditorUtility.SetDirty(this);
        }
        get
        {
            return m_destination;
        }
    }
}