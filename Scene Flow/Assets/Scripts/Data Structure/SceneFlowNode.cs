﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SceneFlowNode : ScriptableObject
{
    [SerializeField]
    private SceneAsset m_scene;

    [SerializeField, HideInInspector]
    private SceneFlowLink[] m_links;

    private SceneFlowSetup m_owner;   

    private void OnEnable()
    {
        if(m_links == null)
        {
            m_links = new SceneFlowLink[0];
        }
    }

    public SceneFlowLink AddLink()
    {
        return AddLink(null);
    }

    public SceneFlowLink AddLink(SceneFlowNode destination)
    {        
        SceneFlowLink link = CreateInstance<SceneFlowLink>();

        link.Destination = destination;
        link.hideFlags = HideFlags.HideInHierarchy;

        Undo.RegisterCreatedObjectUndo(link, "Add link");

        Undo.RegisterCompleteObjectUndo(this, "Add link");

        AssetDatabase.AddObjectToAsset(link, this);

        ArrayUtility.Add(ref m_links, link);

        EditorUtility.SetDirty(this);

        //AssetDatabase.Refresh();

        return link;
    }

    public void RemoveLink(SceneFlowLink link)
    {
        if(ArrayUtility.Contains(m_links, link))
        {
            Undo.RegisterCompleteObjectUndo(this, "delete link");
            ArrayUtility.Remove(ref m_links, link);

            Undo.DestroyObjectImmediate(link);
            EditorUtility.SetDirty(this);
            //DestroyImmediate(link, true);
        }        
    }

    public SceneFlowNode[] GetLinkedScenes()
    {
        List<SceneFlowNode> nodes = new List<SceneFlowNode>();

        for (int i = 0; i < m_links.Length; i++)
        {
            if (m_links[i].Destination != null)
            {                
                nodes.Add(m_links[i].Destination);
            }
        }

        return nodes.ToArray();
    }   

    public SceneFlowLink[] Links
    {
        get
        {
            return m_links;
        }
    }    

    public SceneAsset Scene
    {
        set
        {
            m_scene = value;                                
        }
        get
        {
            return m_scene;
        }
    }
}