﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneHelper : MonoBehaviour
{
    [SerializeField]
    private SceneFlowSetup m_setup;

    [SerializeField]
    private string m_sceneToLoad;
    
    public SceneFlowSetup Setup
    {
        get
        {
            return m_setup;
        }
    }
}