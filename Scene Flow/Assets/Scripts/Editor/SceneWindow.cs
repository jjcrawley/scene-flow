﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System;

public class SceneWindow : EditorWindow
{
    private static SceneWindow s_window;

    private SceneSetup[] m_scenes;
    private Vector2 m_scrollPosition;
    private Vector2 m_otherScrollPosition;
        
    private SceneAsset[] m_allScenes;
    private bool[] m_addToBuild;

    private Undo.UndoRedoCallback undoCallback;

    [MenuItem("Scene Manager/Scene Manager")]
    private static void OpenWindow()
    {
        s_window = GetWindow<SceneWindow>();               
    }
    
    private void UndoRedo()
    {
        Repaint();
    }

    private void Awake()
    {
        //Debug.Log("Called");
        m_scenes = EditorSceneManager.GetSceneManagerSetup();

        //m_allScenes = (SceneAsset[])Resources.FindObjectsOfTypeAll(typeof(SceneAsset));
        m_allScenes = Resources.FindObjectsOfTypeAll<SceneAsset>();
        
        m_addToBuild = new bool[m_allScenes.Length];

        Array.Sort(m_allScenes, new SceneNameComparer());

        EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;

        Debug.Log(m_allScenes.Length);
        Debug.Log(scenes.Length);

        for(int i = 0; i < m_allScenes.Length; i++)
        {
            string path = AssetDatabase.GetAssetOrScenePath(m_allScenes[i]);

            for(int j = 0; j < scenes.Length; j++)
            {
                if(path == scenes[j].path)
                {
                    m_addToBuild[i] = scenes[j].enabled;
                    break; 
                }
            }            
        }
    }

    private void OnEnable()
    {
        undoCallback = UndoRedo;
        Undo.undoRedoPerformed += undoCallback;
    }

    private void OnDisable()
    {
        Undo.undoRedoPerformed -= undoCallback;
    }

    private void OnGUI()
    {
        EditorGUILayout.Space();

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

        EditorGUILayout.LabelField("Current Open Scenes", EditorStyles.largeLabel, GUILayout.Height(21));

        //EditorGUILayout.Space();

        m_scrollPosition = EditorGUILayout.BeginScrollView(m_scrollPosition);

        EditorGUILayout.BeginVertical();

        for(int i = 0; i < m_scenes.Length; i++)
        {
            SceneSetup current = m_scenes[i];
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);

            EditorGUILayout.LabelField(current.path, GUILayout.Width(300));
            EditorGUILayout.LabelField(current.isActive.ToString(), GUILayout.Width(100));

            EditorGUILayout.EndHorizontal();            
        }

        EditorGUILayout.EndVertical();
        EditorGUILayout.EndScrollView();

        EditorGUILayout.EndVertical();

        //GUILayout.FlexibleSpace();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.BeginVertical();
        m_otherScrollPosition = EditorGUILayout.BeginScrollView(m_otherScrollPosition, EditorStyles.helpBox);
        EditorGUILayout.LabelField("All scenes", EditorStyles.largeLabel);
        EditorGUILayout.Space();

        for (int i = 0; i < m_allScenes.Length; i++)
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);

            EditorGUILayout.LabelField(m_allScenes[i].name);
            m_addToBuild[i] = EditorGUILayout.Toggle(m_addToBuild[i]);

            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();               

        if(GUILayout.Button(new GUIContent("Add scenes to build"), EditorStyles.toolbarButton))
        {
            OnSave();
        }      

        GUILayout.FlexibleSpace();
    }

    private void OnSave()
    {
        List<EditorBuildSettingsScene> scenes = new List<EditorBuildSettingsScene>();

        for(int i = 0; i < m_allScenes.Length; i++)
        {
            if(m_addToBuild[i])
            {
                EditorBuildSettingsScene temp = new EditorBuildSettingsScene();

                temp.path = AssetDatabase.GetAssetPath(m_allScenes[i].GetInstanceID());
                temp.enabled = true;
                scenes.Add(temp);
            }
        }

        EditorBuildSettings.scenes = scenes.ToArray();        
    }

    private class SceneNameComparer : IComparer<SceneAsset>
    {
        public int Compare(SceneAsset x, SceneAsset y)
        {
            return x.name.CompareTo(y.name);
        }
    }
}