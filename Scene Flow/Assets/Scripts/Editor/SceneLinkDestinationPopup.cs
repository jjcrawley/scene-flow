﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class SceneLinkDestinationPopup : PopupWindowContent
{
    private SceneFlowSetup m_setup;
    private SceneFlowLink m_link;
    private Vector2 m_scrollPos;

    public delegate void RepaintCallback();
    private RepaintCallback m_onRepaint;
        
    public void SetSceneLink(SceneFlowSetup setup, SceneFlowLink link)
    {
        m_setup = setup;
        m_link = link;
    }

    public override void OnGUI(Rect rect)
    {
        m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);

        SceneFlowNode[] nodes = m_setup.Nodes;

        for(int i = 0; i < nodes.Length; i++)
        {
            if(nodes[i].Scene == null)
            {
                GUILayout.Label("Null scene");
            }
            else if(GUILayout.Button(new GUIContent(nodes[i].Scene.name)))
            {
                m_link.Destination = nodes[i];
                m_onRepaint();
                editorWindow.Close();      
            }
        }

        EditorGUILayout.EndScrollView();
    }

    public void SetRepaintCallback(RepaintCallback callback)
    {
        m_onRepaint = callback;                
    }

    public override Vector2 GetWindowSize()
    {
        return base.GetWindowSize();
    }
}