﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SceneHelper))]
public class SceneHelperEditor : Editor
{
    private SerializedProperty m_sceneField;
    private SerializedProperty m_setupField;

    private int m_selectedIndex;
    private string[] m_connectedScenes;

    private void OnEnable()
    {
        m_sceneField = serializedObject.FindProperty("m_sceneToLoad");
        m_setupField = serializedObject.FindProperty("m_setup");

        SetupConnectedScenes();
    }

    public override void OnInspectorGUI()
    {   
        serializedObject.Update();

        bool changed = false;

        EditorGUI.BeginChangeCheck();

        EditorGUILayout.ObjectField(m_setupField);

        if(EditorGUI.EndChangeCheck())
        {
            changed = true;
        }

        if(m_connectedScenes.Length != 0)
        {
            EditorGUI.BeginChangeCheck();

            m_selectedIndex = EditorGUILayout.Popup("Scene", m_selectedIndex, m_connectedScenes);

            if (EditorGUI.EndChangeCheck())
            {
                m_sceneField.stringValue = m_connectedScenes[m_selectedIndex];
            }            
        }
        else
        {
            EditorGUILayout.LabelField("No scenes connected to this one", EditorStyles.helpBox);            
        }

        serializedObject.ApplyModifiedProperties();

        if (changed)
        {
            SetupConnectedScenes();
        }
    }

    private void SetupConnectedScenes()
    {
        SceneHelper helper = (SceneHelper)target;

        string currentScene = helper.gameObject.scene.name;
        
        if (helper.Setup != null)
        {
            SceneFlowSetup setup = helper.Setup;

            m_connectedScenes = setup.GetScenesLinkedToCurrent(currentScene);

            if (m_sceneField.stringValue == string.Empty)
            {
                if (m_connectedScenes != null && m_connectedScenes.Length > 0)
                {
                    m_sceneField.stringValue = m_connectedScenes[0];

                    serializedObject.ApplyModifiedPropertiesWithoutUndo();
                    m_selectedIndex = 0;
                }
            }
            else
            {
                m_selectedIndex = ArrayUtility.IndexOf(m_connectedScenes, m_sceneField.stringValue);
            }
        }
        else
        {
            m_connectedScenes = new string[0];
        }

        //OnInspectorGUI();
    }
}