﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SceneFlowNode))]
public class SceneFlowNodeEditor : Editor
{
    private Editor[] m_cachedEditors;
    
    private void OnEnable()
    {
        SceneFlowNode node = (SceneFlowNode)target;

        m_cachedEditors = new Editor[node.Links.Length];
        
        for(int i = 0; i < m_cachedEditors.Length; i++)
        {
            m_cachedEditors[i] = CreateEditor(node.Links[i]);
        }                
    }    

    public override void OnInspectorGUI()
    {
        for(int i = 0; i < m_cachedEditors.Length; i++)
        {
            Editor current = m_cachedEditors[i];

            //current.DrawHeader();
            current.OnInspectorGUI();
        }
    }    
}