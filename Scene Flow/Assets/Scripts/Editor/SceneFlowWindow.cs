﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.AnimatedValues;

public class SceneFlowWindow : EditorWindow
{
    private static SceneFlowWindow s_instance;

    private SceneFlowSetup m_setup;

    private SceneAsset[] m_assets;

    private SceneFlowNode[] m_nodes;

    private Vector2 m_scrollPos;
    private Vector2 m_otherScrollPos;
    private AnimBool[] m_foldAnims;

    private Rect m_buttonRect = new Rect();
    private int m_setDestination = -1;
    private int m_nodeIndex = -1;
    private int m_startNodeIndex;
    private SceneFlowLink m_link;

    private Undo.UndoRedoCallback undoCallback;

    [MenuItem("Scene Manager/Scene Flow")]
    private static void InitWindow()
    {
        s_instance = GetWindow<SceneFlowWindow>();                
    }    

    private void OnEnable()
    {
        if(s_instance != null)
        {
            s_instance.Show();
        }

        undoCallback = OnUndoRedo;
        Undo.undoRedoPerformed += undoCallback;
        titleContent = new GUIContent("Scene Flow");
    }
        
    private void OnUndoRedo()
    {
        m_nodes = m_setup.Nodes;
        m_foldAnims = new AnimBool[m_nodes.Length];

        for (int i = 0; i < m_foldAnims.Length; i++)
        {
            m_foldAnims[i] = new AnimBool(false);
            m_foldAnims[i].valueChanged.AddListener(Repaint);
        }
       
        m_assets = m_setup.RegisteredScenes;

        Repaint();
    }

    private void OnDestroy()
    {
        Undo.undoRedoPerformed -= undoCallback;
        //AssetDatabase.Refresh();
        //AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        //Debug.Log("Asset Data base was refreshed");
    }

    public static void InitWindow(SceneFlowSetup setup)
    {
        s_instance = GetWindow<SceneFlowWindow>();        
        s_instance.m_setup = setup;
        s_instance.Init();
        s_instance.Show();
    }

    public void Init()
    {
        //m_assets = m_setup.GetRegisteredScenes;
        m_nodes = m_setup.Nodes;
        m_foldAnims = new AnimBool[m_nodes.Length];

        for (int i = 0; i < m_foldAnims.Length; i++)
        {
            m_foldAnims[i] = new AnimBool(false);
            m_foldAnims[i].valueChanged.AddListener(Repaint);
        }

        titleContent = new GUIContent("Scene Flow");
        UpdateScenes();
        //Debug.Log(m_assets == null);
    }

    private void OnGUI()
    {
        if(m_setup == null)
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.FlexibleSpace();

            EditorGUILayout.HelpBox("No scene setup to look at     ", MessageType.Info);

            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();

            return;
        }

        //Debug.Log("Repainting");
        if (GUILayout.Button(new GUIContent("Update build settings")))
        {
            EditorSettingsWizard wizard = ScriptableWizard.DisplayWizard<EditorSettingsWizard>("Update build settings", "Update", "Quit");
            wizard.Assets = m_setup.RegisteredScenes;
            //UpdateBuildSettings();
        }

        //EditorGUILayout.LabelField("Editing Scenes");

        ////EditorGUILayout.BeginVertical(GUILayout.ExpandHeight(false));

        //m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos, GUILayout.ExpandHeight(false));

        //EditorGUILayout.BeginVertical(GUILayout.ExpandHeight(false));

        //for (int i = 0; i < m_assets.Length; i++)
        //{
        //    if (m_assets[i] != null)
        //    {
        //        EditorGUILayout.LabelField(m_assets[i].name, EditorStyles.helpBox);
        //    }
        //}

        //EditorGUILayout.EndVertical();

        //EditorGUILayout.EndScrollView();

        //Debug.Log(GUILayoutUtility.GetLastRect());

        //EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        if(m_setup.StartNode != null)
        {
            DoNodeGUI(m_setup.StartNode, ArrayUtility.IndexOf(m_nodes, m_setup.StartNode));
        }

        EditorGUILayout.LabelField("Editing nodes");

        if (GUILayout.Button(new GUIContent("Add node")))
        {
            m_setup.AddSceneNode();
            SetupWindowStuff();
        }

        m_otherScrollPos = EditorGUILayout.BeginScrollView(m_otherScrollPos);

        for (int i = 0; i < m_nodes.Length; i++)
        {
            SceneFlowNode node = m_nodes[i];

            if (node != m_setup.StartNode)
            {
                DoNodeGUI(node, i);
            }
            //Debug.Log("Found a node");                   
        }

        EditorGUILayout.EndScrollView();

        HandleDragAndDrop();       
    }

    private void DoNodeGUI(SceneFlowNode node, int i)
    {
        if (node != null)
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.BeginHorizontal();

            EditorGUI.BeginChangeCheck();
            node.Scene = (SceneAsset)EditorGUILayout.ObjectField(node.Scene, typeof(SceneAsset), false);

            if (EditorGUI.EndChangeCheck())
            {
                //m_setup.UpdateSceneAssets();
                UpdateScenes();
                Debug.Log("Hello from node");
                Repaint();
            }            

            EditorGUILayout.EndHorizontal();            

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button(new GUIContent("Add link")))
            {
                node.AddLink();
                m_foldAnims[i].target = true;
            }
            
            if (node != m_setup.StartNode && GUILayout.Button(new GUIContent("Make start")))
            {
                m_setup.StartNode = node;
                UpdateScenes();
            }

            if (node != m_setup.StartNode && GUILayout.Button(new GUIContent("Delete node")))
            {
                RemoveNode(node);
                Repaint();
                return;
            }

            EditorGUILayout.EndHorizontal();

            SetupLinks(node, i);

            EditorGUILayout.Space();

            EditorGUILayout.EndVertical();
        }
    }

    private void SetupLinks(SceneFlowNode node, int i)
    {
        if (node.Links.Length != 0)
        {
            m_foldAnims[i].target = EditorGUILayout.Foldout(m_foldAnims[i].target, "Links");
            //m_folded[i] = EditorGUILayout.Foldout(m_folded[i], "Links");

            if (EditorGUILayout.BeginFadeGroup(m_foldAnims[i].faded))
            {
                for (int j = 0; j < node.Links.Length; j++)
                {
                    SceneFlowLink link = node.Links[j];

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Link");

                    if (link.Destination != null)
                    {
                        EditorGUILayout.LabelField(link.Destination.Scene.name);
                    }

                    if (GUILayout.Button("Set Destination"))
                    {
                        m_link = link;
                        m_setDestination = j;
                        m_nodeIndex = i;
                    }

                    if (m_setDestination == j && m_nodeIndex == i && Event.current.type == EventType.Repaint)
                    {
                        m_buttonRect = GUILayoutUtility.GetLastRect();
                        ShowPopupWindow();
                    }

                    if (GUILayout.Button(new GUIContent("Delete")))
                    {
                        node.RemoveLink(link);
                        Repaint();
                    }

                    EditorGUILayout.EndHorizontal();
                }
            }

            //GUILayout.Space(5);

            EditorGUILayout.EndFadeGroup();
        }
    }

    private void HandleDragAndDrop()
    {
        Rect dragArea = GUILayoutUtility.GetLastRect();

        Event current = Event.current;

        Object[] objects = DragAndDrop.objectReferences;

        int id = GUIUtility.GetControlID(FocusType.Passive);

        if (dragArea.Contains(current.mousePosition))
        {
            switch (current.GetTypeForControl(id))
            {
                case EventType.DragPerform:
                    //Debug.Log("Dragged in");

                    foreach (var item in DragAndDrop.objectReferences)
                    {
                        SceneAsset asset = item as SceneAsset;

                        if (asset != null)
                        {
                            SceneFlowNode node = m_setup.AddSceneNode();
                            node.Scene = asset;
                        }
                    }

                    SetupWindowStuff();
                    //DragAndDrop.AcceptDrag();
                    Repaint();
                    current.Use();

                    break;
                case EventType.DragUpdated:

                    bool accept = true;

                    for (int i = 0; i < objects.Length; i++)
                    {
                        if (objects[i].GetType() != typeof(SceneAsset))
                        {
                            accept = false;
                            break;
                        }
                    }

                    if (accept)
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Link;
                    }
                    else
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
                    }

                    current.Use();

                    break;
            }
        }
    }

    private void UpdateBuildSettings()
    {
        SceneAsset[] assets = m_setup.RegisteredScenes;

        EditorBuildSettingsScene[] settings = new EditorBuildSettingsScene[assets.Length];

        for (int i = 0; i < settings.Length; i++)
        {
            EditorBuildSettingsScene scene = new EditorBuildSettingsScene();
            scene.path = AssetDatabase.GetAssetOrScenePath(assets[i]);
            scene.enabled = true;
            settings[i] = scene;
        }

        EditorBuildSettings.scenes = settings;
    }

    private void RemoveNode(SceneFlowNode node)
    {      
        //int index = ArrayUtility.IndexOf(m_setup.Nodes, node);

        //ArrayUtility.RemoveAt(ref m_foldAnims, index);

        m_setup.RemoveSceneNode(node);
        SetupWindowStuff();
    }

    private void ShowPopupWindow()
    {
        SceneLinkDestinationPopup content = new SceneLinkDestinationPopup();
        content.SetSceneLink(m_setup, m_link);
        content.SetRepaintCallback(Repaint);
        m_setDestination = -1;
        m_nodeIndex = -1;
        m_link = null;
        PopupWindow.Show(m_buttonRect, content);
    }
     
    private void SetupWindowStuff()
    {
        if (m_setup.Nodes.Length != m_nodes.Length)
        {
            m_nodes = m_setup.Nodes;
            m_foldAnims = new AnimBool[m_nodes.Length];

            for (int i = 0; i < m_foldAnims.Length; i++)
            {
                m_foldAnims[i] = new AnimBool(false);
                m_foldAnims[i].valueChanged.AddListener(Repaint);
            }
        }

        m_startNodeIndex = ArrayUtility.IndexOf(m_nodes, m_setup.StartNode);

        UpdateScenes();
    }

    private void UpdateScenes()
    {
        //m_setup.UpdateSceneAssets();
        //m_assets = m_setup.RegisteredScenes;
        //Debug.Log(m_assets.Length);        
    }

    [OnOpenAsset(0)]
    private static bool OpenAsset(int instanceID, int line)
    {
        //Debug.Log(line);

        SceneFlowSetup current = EditorUtility.InstanceIDToObject(instanceID) as SceneFlowSetup;

        if (current != null)
        {
            InitWindow(current);
            return true;
        }

        return false;
    }
}