﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

public class EditorSettingsWizard : ScriptableWizard
{
    private ReorderableList m_list;
    private bool[] m_enabled;

    private Rect m_headerRect; 
    
    private void OnEnable()
    {
        m_list = new ReorderableList(null, typeof(SceneAsset));

        m_list.drawElementCallback = OnDrawListItem;
        m_list.drawHeaderCallback = OnDrawHeader;
        m_list.displayAdd = false;
        m_list.displayRemove = false;
        m_list.draggable = true;
        m_list.footerHeight = 3.0f;
        m_list.headerHeight = 20.0f;

        minSize = new Vector2(150, 0);
    }

    private void OnWizardCreate()
    {
        UpdateBuildSettings();        
    }

    protected override bool DrawWizardGUI()
    {
        if(m_list.list == null)
        {
            EditorGUILayout.HelpBox("No scenes to add to build settings", MessageType.Info);
            return false;
        }

        EditorGUI.BeginChangeCheck();

        //m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);

        m_list.DoLayoutList();

        float height = m_list.GetHeight();

        Vector2 pos = new Vector2();
        pos.x = m_headerRect.width / 2;
        pos.y = m_headerRect.y;

        EditorGUI.DrawRect(new Rect(pos, new Vector2(3, height)), Color.gray);          

        //EditorGUILayout.EndScrollView();

        return EditorGUI.EndChangeCheck();

        //return base.DrawWizardGUI();
    }

    private void OnWizardUpdate()
    {
        helpString = "Setup your build settings";
        Debug.Log("Hello from wizard");
    }

    private void OnDrawListItem(Rect position, int index, bool isActive, bool focused)
    {
        SceneAsset asset = m_list.list[index] as SceneAsset;

        Vector2 heightWidth = EditorStyles.label.CalcSize(new GUIContent(asset.name));
        Rect labelPos = new Rect(position.x + 3, position.y, heightWidth.x, position.height);

        EditorGUI.LabelField(labelPos, new GUIContent(asset.name));

        Rect togglePos = new Rect(position.width / 2 + 23, position.y, 10, position.height);

        m_enabled[index] = EditorGUI.Toggle(togglePos, m_enabled[index]);
    }

    private void OnDrawHeader(Rect position)
    {
        m_headerRect = position;
        float minWidth;
        float maxWidth;

        EditorStyles.label.CalcMinMaxWidth(new GUIContent("Scene"), out minWidth, out maxWidth);

        Rect labelPos = new Rect(position.x + 3, position.y, minWidth, position.height);

        EditorGUI.LabelField(labelPos, "Scene");

        EditorStyles.label.CalcMinMaxWidth(new GUIContent("Add"), out minWidth, out maxWidth);

        labelPos = new Rect(position.width / 2 + 15, position.y, minWidth, position.height);

        EditorGUI.LabelField(labelPos, "Add");                
    }

    private void UpdateBuildSettings()
    {
        EditorBuildSettingsScene[] settings = new EditorBuildSettingsScene[m_list.count];

        for (int i = 0; i < settings.Length; i++)
        {
            EditorBuildSettingsScene scene = new EditorBuildSettingsScene();
            scene.path = AssetDatabase.GetAssetOrScenePath((SceneAsset)m_list.list[i]);
            scene.enabled = m_enabled[i];
            settings[i] = scene;
        }

        EditorBuildSettings.scenes = settings;
    }

    private float GetLongestName(List<SceneAsset> assets)
    {
        float longestWidth = 0;

        for(int i = 0; i < assets.Count; i++)
        {
            float minWidth, maxWidth;
                        
            EditorStyles.label.CalcMinMaxWidth(new GUIContent(assets[i].name), out minWidth, out maxWidth);

            if(minWidth > longestWidth)
            {
                longestWidth = minWidth;
            }
        }

        return longestWidth + 200;
    }

    private void OnWizardOtherButton()
    {
        Close();
    }

    public SceneAsset[] Assets
    {
        set
        {            
            List<SceneAsset> assets = new List<SceneAsset>(value);

            minSize = new Vector2(GetLongestName(assets), 0);

            m_list.list = assets;

            m_enabled = new bool[assets.Count];

            for(int i = 0; i < m_enabled.Length; i++)
            {
                m_enabled[i] = true;
            }

            Repaint();
        }
    }
}